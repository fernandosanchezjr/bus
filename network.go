package bus

import (
	"fmt"
	"sync"
)

type Network struct {
	buses     map[int]*Bus
	clients   map[int]*Client
	waitGroup sync.WaitGroup
}

func NewNetwork() *Network {
	n := &Network{buses: make(map[int]*Bus), clients: make(map[int]*Client)}
	return n
}

func (n *Network) SetBus(i int, b *Bus) {
	n.buses[i] = b
	b.Id = i
	b.waitGroup = &n.waitGroup
	b.Start()
}

func (n *Network) GetBus(bus int) (*Bus, error) {
	busConnection, ok := n.buses[bus]
	if !ok {
		return nil, fmt.Errorf("client %d not found", bus)
	}
	return busConnection, nil
}

func (n *Network) RemoveBus(bus int) error {
	busConnection, ok := n.buses[bus]
	if !ok {
		return fmt.Errorf("bus %d not found", bus)
	}
	busConnection.Stop()
	delete(n.buses, bus)
	return nil
}

func (n *Network) BusCount() int {
	return len(n.buses)
}

func (n *Network) SetClient(i int, c *Client) {
	n.clients[i] = c
	c.Id = i
	c.waitGroup = &n.waitGroup
	c.Start()
}

func (n *Network) GetClient(client int) (*Client, error) {
	clientConnection, ok := n.clients[client]
	if !ok {
		return nil, fmt.Errorf("client %d not found", client)
	}
	return clientConnection, nil
}

func (n *Network) RemoveClient(client int) error {
	clientConnection, ok := n.clients[client]
	if !ok {
		return fmt.Errorf("client %d not found", client)
	}
	clientConnection.Stop()
	delete(n.clients, client)
	return nil
}

func (n *Network) ClientCount() int {
	return len(n.clients)
}

func (n *Network) Connect(bus int, client int) error {
	busConnection, ok := n.buses[bus]
	if !ok {
		return fmt.Errorf("bus %d not found", bus)
	}
	clientConnection, ok := n.clients[client]
	if !ok {
		return fmt.Errorf("client %d not found", bus)
	}
	return busConnection.Subscribe(clientConnection)
}

func (n *Network) Disconnect(bus int, client int) error {
	busConnection, ok := n.buses[bus]
	if !ok {
		return fmt.Errorf("bus %d not found", bus)
	}
	clientConnection, ok := n.clients[client]
	if !ok {
		return fmt.Errorf("client %d not found", bus)
	}
	return busConnection.Unsubscribe(clientConnection)
}

func (n *Network) MessageBus(bus int, message interface{}) error {
	busConnection, ok := n.buses[bus]
	if !ok {
		return fmt.Errorf("bus %d not found", bus)
	}
	busConnection.Message(message)
	return nil
}

func (n *Network) MessageClient(client int, message interface{}) error {
	clientConnection, ok := n.clients[client]
	if !ok {
		return fmt.Errorf("client %d not found", client)
	}
	clientConnection.Message(message)
	return nil
}

func (n *Network) Wait() {
	n.waitGroup.Wait()
}