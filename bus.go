package bus

import (
	"errors"
	"reflect"
	"sync"
)

type Bus struct {
	messageType     reflect.Type
	channel         chan interface{}
	shutdownChannel chan int
	subscribers     []*Client
	waitGroup       *sync.WaitGroup
	Id              int
}

func NewBus(messageType reflect.Type) *Bus {
	b := &Bus{messageType: messageType, channel: nil,
		shutdownChannel: make(chan int),
		subscribers:     make([]*Client, 0)}
	return b
}

func (b *Bus) startOperation() {
	if b.waitGroup != nil {
		b.waitGroup.Add(1)
	}
}

func (b *Bus) operationComplete() {
	if b.waitGroup != nil {
		b.waitGroup.Done()
	}
}

func (b *Bus) Start() {
	b.startOperation()
	b.channel = make(chan interface{})
	go b.mainLoop()
}

func (b *Bus) Stop() {
	b.startOperation()
	if b.channel == nil {
		return
	}
	b.shutdownChannel <- 0
}

func (b *Bus) mainLoop() {
	b.operationComplete()
	shutdownRequested := false
	for {
		select {
		case msg := <-b.channel:
			for _, c := range b.subscribers {
				c.Message(msg)
			}
			b.operationComplete()
			if len(b.channel) == 0 && shutdownRequested == true {
				b.operationComplete()
				b.waitGroup = nil
				return
			}
		case <-b.shutdownChannel:
			shutdownRequested = true
			if len(b.channel) == 0 {
				b.channel = nil
				b.operationComplete()
				b.waitGroup = nil
				return
			}
		}
	}
}

func (b *Bus) Subscribe(c *Client) error {
	if b.messageType.String() != c.messageType.String() {
		return errors.New("message type mismatch between bus and client")
	}
	b.subscribers = append(b.subscribers, c)
	return nil
}

func (b *Bus) Unsubscribe(c *Client) error {
	if b.messageType.String() != c.messageType.String() {
		return errors.New("message type mismatch between bus and client")
	}
	newSubscribers := make([]*Client, 0)
	for _, s := range b.subscribers {
		if s != c {
			newSubscribers = append(newSubscribers, s)
		}
	}
	b.subscribers = newSubscribers
	return nil
}

func (b *Bus) Message(message interface{}) {
	b.startOperation()
	b.channel <- message
}
