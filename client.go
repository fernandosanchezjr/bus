package bus

import (
	"errors"
	"fmt"
	"reflect"
	"sync"
)

type Client struct {
	messageType     reflect.Type
	channel         chan interface{}
	shutdownChannel chan int
	handlers        []reflect.Value
	waitGroup       *sync.WaitGroup
	Id              int
}

func NewClient(messageType reflect.Type) *Client {
	println(messageType.String())
	c := &Client{messageType: messageType, channel: nil,
		shutdownChannel: make(chan int, 10), handlers: make([]reflect.Value, 0)}
	return c
}

func (c *Client) startOperation() {
	if c.waitGroup != nil {
		c.waitGroup.Add(1)
	}
}

func (c *Client) operationComplete() {
	if c.waitGroup != nil {
		c.waitGroup.Done()
	}
}

func (c *Client) Start() {
	c.startOperation()
	c.channel = make(chan interface{})
	go c.mainLoop()
}

func (c *Client) Stop() {
	c.startOperation()
	if c.channel == nil {
		return
	}
	c.shutdownChannel <- 0
}

func (c *Client) mainLoop() {
	c.operationComplete()
	shutdownRequested := false
	for {
		select {
		case msg := <-c.channel:
			params := []reflect.Value{reflect.ValueOf(c.Id), reflect.ValueOf(msg)}
			for _, h := range c.handlers {
				h.Call(params)
			}
			c.operationComplete()
			if len(c.channel) == 0 && shutdownRequested == true {
				c.operationComplete()
				c.waitGroup = nil
				return
			}
		case <-c.shutdownChannel:
			shutdownRequested = true
			if len(c.channel) == 0 {
				c.channel = nil
				c.operationComplete()
				c.waitGroup = nil
				return
			}
		}
	}
}

func (c *Client) AddHandler(f interface{}) error {
	intType := reflect.TypeOf(int(0))
	fValue := reflect.ValueOf(f)
	if fValue.Kind() != reflect.Func {
		return errors.New("handler must be a function")
	}
	if fValue.Type().NumIn() != 2 {
		return fmt.Errorf("handler must be a func receiving an integer id and a %s value",
			fValue.Type().String())
	}
	if fValue.Type().In(0).String() != intType.String() {
		return errors.New("first func param must be int")
	}
	if fValue.Type().In(1).String() != c.messageType.String() {
		println(fValue.Type().In(1).String(), c.messageType.String())
		return fmt.Errorf("second func param must be %s", c.messageType.String())
	}
	c.handlers = append(c.handlers, fValue)
	return nil
}

func (c *Client) Message(message interface{}) {
	c.startOperation()
	c.channel <- message
}
