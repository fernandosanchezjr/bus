package bus

import (
	"reflect"
	"testing"
)

type testMessage struct {
	a int
	b string
}

type nonTestMessage struct {
	a int
	b float32
}

func testHandler(id int, t *testMessage) {
	println("Received", t, "from", id)
}

func nonTestHandler(id int, t *nonTestMessage) {
	println("Received", t, "from", id)
}

func TestNewBus(t *testing.T) {
	b := NewBus(reflect.TypeOf(&testMessage{}))
	if b == nil {
		t.FailNow()
	}
}

func TestBusStartStop(t *testing.T) {
	b := NewBus(reflect.TypeOf(&testMessage{}))
	b.Start()
	b.Stop()
}

func TestBusSubscribeUnsubscribe(t *testing.T) {
	b := NewBus(reflect.TypeOf(&testMessage{}))
	c := NewClient(reflect.TypeOf(&testMessage{}))
	d := NewClient(reflect.TypeOf(&nonTestMessage{}))
	if err := b.Subscribe(c); err != nil {
		t.FailNow()
	}
	if len(b.subscribers) != 1 {
		t.FailNow()
	}
	b.Unsubscribe(c)
	if len(b.subscribers) != 0 {
		t.FailNow()
	}
	if err := b.Subscribe(d); err == nil {
		t.FailNow()
	}
}

func TestClientAddHandler(t *testing.T) {
	c := NewClient(reflect.TypeOf(&testMessage{}))
	if err := c.AddHandler(testHandler); err != nil {
		t.FailNow()
	}
	if err := c.AddHandler(nonTestHandler); err == nil {
		t.FailNow()
	}
}

func TestBusMessage(t *testing.T) {
	b := NewBus(reflect.TypeOf(&testMessage{}))
	c := NewClient(reflect.TypeOf(&testMessage{}))
	c.AddHandler(testHandler)
	if err := b.Subscribe(c); err != nil {
		t.FailNow()
	}
	b.Start()
	c.Start()
	b.Message(&testMessage{a: 1, b: "hello"})
	b.Stop()
}

func TestNewNetwork(t *testing.T) {
	n := NewNetwork()
	if n == nil {
		t.FailNow()
	}
}

func TestNetworkSetDeleteBus(t *testing.T) {
	n := NewNetwork()
	b := NewBus(reflect.TypeOf(&testMessage{}))
	n.SetBus(0, b)
	n.RemoveBus(0)
	n.Wait()
}

func TestNetworkSetDeleteClient(t *testing.T) {
	n := NewNetwork()
	c := NewClient(reflect.TypeOf(&testMessage{}))
	n.SetClient(0, c)
	n.RemoveClient(0)
	n.Wait()
}

func TestNetworkConnectDisconnect(t *testing.T) {
	n := NewNetwork()
	b := NewBus(reflect.TypeOf(&testMessage{}))
	n.SetBus(0, b)
	c := NewClient(reflect.TypeOf(&testMessage{}))
	if err := c.AddHandler(testHandler); err != nil {
		t.FailNow()
	}
	n.SetClient(1, c)
	n.Connect(0, 1)
	n.MessageBus(0, &testMessage{a: 2, b: "world"})
	n.MessageClient(1, &testMessage{a: 3, b: "house"})
	n.Wait()
}
